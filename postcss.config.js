module.exports = {
  plugins: {
    tailwindcss: {
      config: {
        theme: {
          extend: {
            colors: {
              DkBlue : '#000A1A',
              DgBlue : '#00173D',
              DbBlue : '#43597D',
              DgGray: '#4B586F',
              DAGray : '#93A1B8',
              DgGray : '#6A6A6A',
              DbGray : '#4F4F4F',
              DrGray : '#F3F3F3',
              DlGray : '#323232'
            },
            spacing: {
              '800': '80rem',
              '675' : '67.5625rem',
              '505': '50.5rem',
              '472': '47.25rem',
              '450': '45rem',
              '400': '40rem',
              '380' : '38rem',
              '360' : '36.625rem',
              '331' : '35rem',
              '345' : '39.5rem',
              '320' : '32rem',
              '327' : '32.75rem',
              '250' : '25.2rem',
              '220' : '22rem',
              '50' : '12.4375rem',
              '38' : '9.375rem',
              '30': '7.563rem',
              '26' : '6.625rem',
              '22' : '5.5rem'
            },
            fontFamily: {
              'lato': ['Lato', 'sans-serif'],
             },
             minWidth: {
              '50' : '12.4375rem',
              '44' : '11rem',
             },
             boxShadow: {

              '3xl': '0 -25px 50px -12px rgba(0, 0, 0, 0.1 )',
              '4xl': '0px 2px 4px rgba(0, 0, 0, 0.25)',
            }
          },
        },
      }
    },
    autoprefixer: {},
  },
}
